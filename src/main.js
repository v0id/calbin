import App from './App'
import './global.styl'

import runtime from 'serviceworker-webpack-plugin/lib/runtime'

import actualizacionDisponible from './actualizacionDisponible'

async function registerServiceWorker() {
  if ('serviceWorker' in navigator) {
    const reg = await runtime.register()
    console.log(reg)

    function listenForWaitingServiceWorker(reg, callback) {
      function awaitStateChange() {
        reg.installing.addEventListener('statechange', function () {
          if (this.state === 'installed') callback(reg)
        })
      }
      if (!reg) return
      if (reg.waiting && reg.active) return callback(reg)
      // si se está instalando una actualización
      if (reg.installing && reg.active) awaitStateChange()
      if (reg.active) reg.addEventListener('updatefound', awaitStateChange)
    }

    let refreshing
    navigator.serviceWorker.addEventListener('controllerchange', () => {
      if (refreshing) return
      refreshing = true
      window.location.reload()
    })

    function promptUserToRefresh(reg) {
      window.__registration = reg
      actualizacionDisponible.update(() => true)
      console.log('prompted')
    }

    listenForWaitingServiceWorker(reg, promptUserToRefresh)
  }
}
process.env.NODE_ENV === 'production' && registerServiceWorker()

const app = new App({
  target: document.body,
})

window.app = app

export default app
