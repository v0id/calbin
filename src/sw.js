const finalAssets = [
  ...serviceWorkerOption.assets,
  '/',
]

console.log('got assets:', serviceWorkerOption.assets)
console.log('final assets:', finalAssets)

self.addEventListener('install', e => {
  const CACHE_NAME = 'calendario-binario-assets-' + new Date().toISOString()
  e.waitUntil(
    caches
      .keys()
      .then(names =>
        names.forEach(name => caches.delete(name)),
      )
      .then(() => caches.open(CACHE_NAME))
      .then(cache => Promise.all(finalAssets.map(asset => cache.add(asset)))),
  )
})

self.addEventListener('fetch', event => {
  console.log('serving from sw:', event.request.url)

  if (event.request.method !== 'GET') return

  event.respondWith(
    caches.match(event.request).then(response => {
      if (response) console.log('serving from cache:', event.request.url)
      return response || fetch(event.request)
    }),
  )
})

self.addEventListener('message', messageEvent => {
  if (messageEvent.data === 'skipWaiting') return skipWaiting()
})
